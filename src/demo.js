if(!!(window.addEventListener)) window.addEventListener('DOMContentLoaded', main);
else window.attachEvent('onload', main);

function main() {
    lineChart();
    barsChart();
    pieChart();
    doughnutChart();
    polarArea();
    radarArea();
}

function lineChart() {
    var data = {
        // Datas
        labels : ["9/1","16/1","30/1*","13/2-15/2","20/2"],
        datasets : [
            {
            fillColor : "rgba(177,6,6,0.5)",
            strokeColor : "rgba(177,6,6,1)",
            pointColor : "rgba(177,6,6,1)",
            pointStrokeColor : "#fff",
            // Alterar aqui
            data : [2,1,0,11,3],
            label : 'Bruno'
        },
        {
            fillColor : "rgba(252,138,7,0.5)",
            strokeColor : "rgba(252,138,7,1)",
            pointColor : "rgba(252,138,7,1)",
            pointStrokeColor : "#fff",
            // Alterar aqui
            data : [1,8,0,9,3],
            label : 'Bianca'
        }
        ]
    };

    var ctx = document.getElementById("lineChart").getContext("2d");
    new Chart(ctx).Line(data);

    legend(document.getElementById("lineLegend"), data);
}